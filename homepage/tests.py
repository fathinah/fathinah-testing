from django.test import TestCase
from django.http import HttpRequest
from django.urls import resolve
from django.test import Client

from .models import Message
from .forms import MessageForm
from django.utils import timezone

from . import views

from selenium import webdriver
from django.test import LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest


# Create your tests here.
class SampleTest(TestCase):
    def test_homepage(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.home)

    def test_status_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage/index.html')

    def test_kabar(self):
        response = Client().get('/')
        self.assertIn('Welcome to the club!', response.content.decode())

    def test_create_new_model(self):
        msg = Message.objects.create(time=timezone.now(), message="Gewd")
        count_msg = Message.objects.all().count()

        self.assertEqual(count_msg, 1)

    def test_form_validated(self):
        form = MessageForm(data={'message': '', 'time': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['Message'],
            ['This field is required.']
        )

    def test_message_completed(self):
        test_str = 'Saya senang'
        response_post = Client().post('/', {'pesan': test_str, 'waktu': timezone.now})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/')
        html_response = response.content.decode('UTF-8')


class MySeleniumTests(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        self.selenium.get(self.live_server_url)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_browser_open(self):
        self.assertIn('Thina\'s Page', self.selenium.title)
        time.sleep(3)

    def test_dev_online_present(self):
        dev = self.selenium.find_element_by_id("online_stat").text
        time.sleep(3)
        self.assertEquals(dev, "Developer's Online Status")

    def test_inputting_to_input(self):
        message = "Smile"
        text_area = self.selenium.find_element_by_id("msg_form")
        text_area.send_keys(message)
        time.sleep(3)
        sub_button = self.selenium.find_element_by_id("tombol")
        sub_button.click()
        time.sleep(3)
        self.assertIn(message, self.selenium.page_source)

    # story nih
    def test_akordion_aktivitas_ada(self):
        selenium = self.selenium
        a_href = selenium.find_element_by_id("abt")
        a_href.click()
        time.sleep(1)
        accordion = selenium.find_element_by_class_name("accordion-header").text
        self.assertEquals(accordion, "Activities")

    def test_button_muncul(self):
        selenium = self.selenium
        a_href = selenium.find_element_by_id("abt")
        a_href.click()
        buttonq = selenium.find_element_by_class_name("lbl").text
        self.assertEquals(buttonq, "swipe")
