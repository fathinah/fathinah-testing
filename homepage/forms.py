from django import forms
from .models import Message


class MessageForm(forms.Form):
    Message = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form',
        'id': 'msg_form',
        'required': 'True',
        'placeholder': 'How are you today?',
    }))
