from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm
from datetime import datetime


# Create your views here.


def home(request):
    data = Message.objects.all().values()
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            status = Message()
            status.message = form.cleaned_data['Message']
            status.time = datetime.now()
            status.save()
            return HttpResponseRedirect('/')
    else:
        form = MessageForm()

    response = {
        'form': form,
        'data': data,
    }

    return render(request, 'homepage/index.html', response)


def about(request):
    return render(request, 'homepage/activities.html', {})
