from django.db import models


# Create your models here.

class Message(models.Model):
    time = models.DateTimeField(auto_now_add=True, blank=True)
    message = models.TextField(max_length=300)
