from django.urls import path
from .views import index, logout_view

appname = 'login_project'

urlpatterns = [
    path('', index, name='startPage'),
    path('logout', logout_view, name='logout'),
]
