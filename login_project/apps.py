from django.apps import AppConfig


class LoginProjectConfig(AppConfig):
    name = 'login_project'
