from django.test import TestCase, Client
import unittest
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import index
import time


class Story9(TestCase):

    def test_page(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_page_template(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'login_page.html')

    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Welcome", html_response)
