from django.shortcuts import render, redirect
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout


def index(request):
    form = LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            passw = form.cleaned_data['password']
            user = authenticate(request, username=uname, password=passw)
            if user is not None:
                login(request, user)
                response = {'form': form, 'user': user}
                return redirect('/login')
            else:
                response = {'form': form, 'user': user}
                return redirect('/login')
    else:
        response = {'form': form}
        return render(request, 'login_page.html', response)


def logout_view(request):
    logout(request)
    form = LoginForm()
    response = {'form': form}
    return redirect('/login')
