$(document).ready(function () {

    $(".loader")
        .delay(100)
        .fadeOut();

    $(".accordion").on("click", ".accordion-header", function () {
        $(this)
            .toggleClass("active")
            .next()
            .slideToggle();
    });

    var checkbox = document.querySelector('input[name=theme]');

    checkbox.addEventListener('change', function () {
        if (this.checked) {
            trans()
            document.documentElement.setAttribute('data-theme', 'dark')
        } else {
            trans()
            document.documentElement.setAttribute('data-theme', 'light')
        }
    })

    let trans = () => {
        document.documentElement.classList.add('transition');
        window.setTimeout(() => {
            document.documentElement.classList.remove('transition')
        }, 1000)
    }

});
